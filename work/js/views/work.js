'use strict';

angular.module('abcd').controller('WorkCtrl', ['$scope', function ($scope) {
	$scope.portfolio = [{
		title: 'foo',
		description: 'foodesc',
		youtubeID: '11111'
	}, {
		title: 'bar',
		description: 'bardesc',
		youtubeID: '22222'
	}, {
		title: 'foobar',
		description: 'fooBar',
		youtubeID: '33333'
	}];
	console.log('WorkCtrl');
}]);
//# sourceMappingURL=WorkCtrl.js.map