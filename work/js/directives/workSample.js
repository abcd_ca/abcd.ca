'use strict';

angular.module('abcd').directive('workSample', [function () {
	return {
		restrict: 'EA',
		template: '\n\t\t\t<div class="work-sample">\n\t\t\t\t<div class="embed-responsive embed-responsive-16by9">\n\t\t\t\t\t<!--<div ob-lazytube="{{item.youtubeID}} ob-placeholder="{{item.previewImage}}">{{item.title}}</div>-->\n\t\t\t\t\t<div ob-lazytube="{{item.youtubeID}}" ob-placeholder="{{item.placeholder}}">{{item.title}}</div>\n\t\t\t\t\t<!--<youtube-video class="embed-responsive-item"-->\n\t\t\t\t\t               <!--video-id="item.youtubeID"-->\n\t\t\t\t\t               <!--player-vars="playerVars"></youtube-video>-->\n\t\t\t\t</div>\n\t\t\t\t<h3 class="title">{{item.title}}</h3>\n\t\t\t\t<p class="description">{{item.description}}</p>\n\t\t\t</div>\n\t\t\t',

		replace: true,
		scope: {
			item: "=",
			playerVars: "="
		},

		link: function link(scope, element, attrs) {}
	};
}]);
//# sourceMappingURL=workSample.js.map