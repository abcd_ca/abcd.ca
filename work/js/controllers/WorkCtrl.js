'use strict';

angular.module('abcd').controller('WorkCtrl', ['$scope', function ($scope) {

	$scope.portfolio = [

	//Knowledge Network, Mobile App
	{
		title: 'Knowledge Network | Mobile video player app',
		description: 'A mobile app for iOS and Android, phone and tablets. Like the Netflix app but for Knowlege Network, the public broadcast TV channel of B.C., Canada',
		youtubeID: 'eH-lbdYZbWY',
		placeholder: '/work/img/videoThumbnails/knet.png'
	},

	//Billion Oyster Project
	{
		title: 'Billion Oyster Project | Sentinel dashboard',
		description: 'A mobile app for iOS and Android, phone, tablets and desktop browsers. Used by scientists, teachers and students to monitor the health of oysters and their surrounding environment in New York harbour. The Billion Oyster Project aims to put a billion oysters in the waters over time, to naturally clean up years of industrial pollution.',
		youtubeID: 'iqgDKY-2znY',
		placeholder: '/work/img/videoThumbnails/bop.png'
	},

	//Wheels, Sentinel
	{
		title: 'Wheels, Inc. | Sentinel dashboard',
		description: 'A reporting dashboard for users who are vehicle fleet managers at their companies. Built in Sencha Touch and optimized for iPad, iPhone, Kindle Fire and Chrome on desktop.',
		youtubeID: 'lReEag-HCno',
		placeholder: '/work/img/videoThumbnails/wheels.png'
	},

	//station square
	{
		title: 'Station Square Touch Screen Installation',
		description: 'Interactive showroom touch screen built for a large real estate development called Station Square outside of Vancouver. Single interactive presentation bult for display on eight large flat screen displays.',
		youtubeID: 'aRk5oXkiWEg',
		placeholder: '/work/img/videoThumbnails/station-square.png'
	},

	//Leon Lebeniste
	{
		title: 'Leon Lebeniste | Manufacturing Execution Software (MES)',
		description: 'Mobile app and desktop portal created to increase productivity and save costs for a fine furnishing and architectural woodwork company. Each architectural part gets a QR Code sticker, printed via the desktop version of the app. Mobile app scans QR codes for parts at every point in the process giving much better visibility into the status of a part and a project as a whole. Problems with the parts are easily surfaced for quick resolution. Parts are imported from architectural programs or entered manually.',
		youtubeID: 'MF1Z4Y0FZvE',
		placeholder: '/work/img/videoThumbnails/llMES.png'
	},

	//Ryder mobile app
	{
		title: 'Ryder Mobile App',
		description: 'Mobile app built for Ryder. Business Goal: Make trucks easy to share, affording fleet owners the opportunity to generate revenue from their idle vehicles and providing businesses with an alternative source of rental vehicles.',
		youtubeID: 'dno4L-GCD9Q',
		placeholder: '/work/img/videoThumbnails/ryder-coop.png'
	},

	//RESAAS mobile app
	{
		title: 'RESAAS Mobile App',
		description: 'iOS and Android mobile app built for RESASS; a social network for real estate professionals.',
		youtubeID: 'O5PQOjh6lf8',
		placeholder: '/work/img/videoThumbnails/resass.png'
	},

	//orkin pest project
	{
		title: 'Orkin Pest Project',
		description: 'Parallax scrolling site whose host is a honey bee which flies along to guide you when you scroll. Regions were set aside where the bee was not to fly over. Organic flight path was created so that the animation appeared as natural and random as possible, achieved with a spline path whose shape morphs to avoid predetermined obstacles (mainly text content). Also includes a reusable "arc nav" component.',
		youtubeID: 'ISW927ZZphk',
		placeholder: '/work/img/videoThumbnails/orkin.png'
	},

	//disney summer quest
	{
		title: 'Disney Summer Quest for 1000 Prizes',
		description: 'A summer-long campaign, kids sign up and are assigned to a team. Every day when watching morning cartoons a password is presented between commercials. The password is made up of fun icons. They must remember this password and then enter it on the website. If they get it correct, they improve their team\'s chances for advancement. After each day the previous day\'s results are shown in the form of a 3D animated board game where teams battle it out and sometimes get set back or swap places with other teams due to surprise booby traps!',
		youtubeID: 'auNbJrs-a2Q',
		placeholder: '/work/img/videoThumbnails/disney.png'
	},

	//Jordan regimen builder
	{
		title: 'Jordan Regimen Builder',
		description: 'A (Michael) Jordan website designed for aspiring athletes at the high school level. Users can choose to have a workout regimen prescribed to them based on a self or peer evaluation and can also customize their own. Regimens consist of a series of workout videos which can be downloaded for offline viewing.',
		youtubeID: 'kEsPBeVshtY',
		placeholder: '/work/img/videoThumbnails/jordan.png'
	},

	//Media Connect
	{
		title: 'MLS realtor photo tool',
		description: 'Designed to improve productivity and to offload image processing to the client. Includes client-side manipulations which include scale, rotate/crop. Images are scaled down (if needed) and JPEG encoded on the client before being uploaded to the server.',
		youtubeID: 'VpB6uyDDBiY',
		placeholder: '/work/img/videoThumbnails/MRIS-mediaConnect.png'
	},

	//Nike timing
	{
		title: 'Nike Timing',
		description: 'Feature\'s Nike\'s line of watches. Localized for at least 8 languages including Japanese.',
		youtubeID: 'wEkxyVDDaMk',
		placeholder: '/work/img/videoThumbnails/nike-timing.png'
	},

	//Nike running
	{
		title: 'Nike Running Canada',
		description: 'Site for Nike\'s line of running apparel products, and dovetailed with Nike+ -- the iPod accessory. Also includes information about running events and meetup locations across Canada.',
		youtubeID: 'dhq40vpFVBk',
		placeholder: '/work/img/videoThumbnails/nike-running.png'
	},

	//VW Kiosk
	{
		title: 'Volkswagen Kiosk',
		description: 'Kiosk for Volkswagen auto showroom used to promote a GPS navigation system. Users can pick some places of interest on the kiosk then save them to an SD card. Kiosk senses insertion of SD card and saves user\'s places. User can then insert the SD card into a car in the showroom to import their saved locations. Includes an embedded webkit browser to repurpose part of the site that also exists on a website that a user can use at home to do some of the things the kiosk does.',
		youtubeID: 'DDFup7CTWiw',
		placeholder: '/work/img/videoThumbnails/vw-kiosk.png'
	},

	//CTCA
	{
		title: 'Cancer Treatment Centers of America (CTCA)',
		description: 'Website shares the stories of cancer survivors from the context of their network of loved ones and care givers. Custom math-based animation used to introduce the networks of people from the perimeter of the circle.',
		youtubeID: 'HPmWs-XIgKM',
		placeholder: '/work/img/videoThumbnails/ctca.png'
	},

	////Mentor Love Your Look 3D Modeller
	//{
	//	title:'Mentor "Love Your Look" 3D Modeller App',
	//	description:'This app is targeted at women considering breast implant surgery including post-mastectomy reconstruction. It aims to give users a rough preview of their surgical outcome and serves as a communication tool between patients and doctors. Mentor Worldwide LLC is a leading supplier of medical products for the global aesthetic medicine market.',
	//	youtubeID:'iwSTrYmk_HI'
	//},

	//Greenies GLM
	{
		title: 'Greenies GLM',
		description: 'Story of the Green Lipped Mussel dog treats',
		youtubeID: 'IWKoasVcKEQ',
		placeholder: '/work/img/videoThumbnails/greenies.png'

	},

	//edgemere
	{
		title: 'Edgemere Private Residences',
		description: 'Housing development built on an old estate property in Oakville, Ontario Canada. Includes responsive layout when browser window is resized.',
		youtubeID: 'a2y32zhiSBc',
		placeholder: '/work/img/videoThumbnails/edgemere.png'
	},

	//reelhouse
	{
		title: 'Reelhouse.org video player',
		description: 'Flash version of Reelhouse video player (They also have an HTML5 one). This one is used in FireFox and in Facebook instances. Very customizable from its colours to which social sharing icons it uses and supports videos of various sizes and aspect ratios.',
		youtubeID: 'iBRmCTQ_LtU',
		placeholder: '/work/img/videoThumbnails/reelhouse.png'
	}];

	//https://github.com/brandly/angular-youtube-embed#player-parameters
	//https://developers.google.com/youtube/player_parameters
	$scope.playerVars = {
		controls: 2,
		modestbranding: true,
		rel: 0,
		showinfo: false
	};
}]);
//# sourceMappingURL=WorkCtrl.js.map