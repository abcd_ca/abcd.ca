'use strict';

// Declare app level module which depends on views, and components
angular.module('abcd', [
	'ngRoute',
	'youtube-embed', //deprecated
	'oblador.lazytube'
]).
config(['$routeProvider', function ($routeProvider) {
	$routeProvider.otherwise({redirectTo: '/'})

	.when('/', {
		templateUrl: '/work/templates/work.html',
		controller: 'WorkCtrl'
	});
}])
.run(function (obLazytubeConfig) {

	//Change default player size
	//obLazytubeConfig.width = 800;
	//obLazytubeConfig.height = 600;

	//Disable related videos
	obLazytubeConfig.urlParams.rel = 0;

	//Disable responsive player
	//obLazytubeConfig.responsive = false;
});
