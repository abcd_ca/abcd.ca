angular.module('abcd')
.directive('workSample', [
	() => {
		return {
			restrict: 'EA',
			template: `
			<div class="work-sample">
				<div class="embed-responsive embed-responsive-16by9">
					<!--<div ob-lazytube="{{item.youtubeID}} ob-placeholder="{{item.previewImage}}">{{item.title}}</div>-->
					<div ob-lazytube="{{item.youtubeID}}" ob-placeholder="{{item.placeholder}}">{{item.title}}</div>
					<!--<youtube-video class="embed-responsive-item"-->
					               <!--video-id="item.youtubeID"-->
					               <!--player-vars="playerVars"></youtube-video>-->
				</div>
				<h3 class="title">{{item.title}}</h3>
				<p class="description">{{item.description}}</p>
			</div>
			`,

			replace: true,
			scope: {
				item: "=",
				playerVars: "="
			},

			link(scope, element, attrs) {

			}
		};
	}
]);
